package io.choerodon.workflow.infra.mapper;

import io.choerodon.mybatis.common.BaseMapper;
import io.choerodon.workflow.domain.entity.ProjectWorkflowRel;

/**
 * @author zhaotianxin
 * @date 2021-03-08 19:00
 */
public interface ProjectWorkflowRelMapper extends BaseMapper<ProjectWorkflowRel> {
}
