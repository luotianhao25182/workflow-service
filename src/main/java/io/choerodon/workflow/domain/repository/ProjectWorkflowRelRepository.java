package io.choerodon.workflow.domain.repository;

import io.choerodon.workflow.domain.entity.ProjectWorkflowRel;

import org.hzero.mybatis.base.BaseRepository;

public interface ProjectWorkflowRelRepository extends BaseRepository<ProjectWorkflowRel> {
}
