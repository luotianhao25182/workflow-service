package io.choerodon.workflow.domain.repository;

import io.choerodon.workflow.domain.entity.BacklogInstanceRel;

import org.hzero.mybatis.base.BaseRepository;

public interface BacklogInstanceRelRepository extends BaseRepository<BacklogInstanceRel> {
}
